package com.zad5;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RegularExpression {

     String znajdz(String text, String format) {
        Pattern pattern = Pattern.compile(format);
        Matcher matcher = pattern.matcher(text);
        String result = null;
        while(matcher.find()) {
            result = matcher.group();
        }
        return result;
    }

}

class XMLManager {

    RegularExpression r = new RegularExpression();

    void createXMLFile(String text, String xmlFilePath) {
        String[] lines = text.split(System.getProperty("line.separator"));
        System.out.println(text);

        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            //zawartosc
            Element root = document.createElement("Faktura");
            document.appendChild(root);

            Element nabywcaNip = document.createElement("NabywcaNIP");
            root.appendChild(nabywcaNip);

            Element sprzedawcaNip = document.createElement("SprzedawcaNIP");
            root.appendChild(sprzedawcaNip);

            Element nrFaktury = document.createElement("nrFaktury");
            root.appendChild(nrFaktury);

            Element vat = document.createElement("VAT");
            root.appendChild(vat);

            Element data = document.createElement("Data");
            root.appendChild(data);

            Element termin = document.createElement("Termin");
            root.appendChild(termin);

            Element cenaBrutto = document.createElement("CenaBrutto");
            root.appendChild(cenaBrutto);

            Element kontoBankowe = document.createElement("KontoBankowe");
            root.appendChild(kontoBankowe);

            //VAT
            System.out.println("Vat: " + r.znajdz(text, "\\d{2}\\%"));
            vat.appendChild(document.createTextNode(r.znajdz(text, "\\d{2}\\%")));

            //NR faktury
            System.out.println("Numer faktury: " + r.znajdz(text, "\\bFAKTURA VAT NR+.*"));
            nrFaktury.appendChild(document.createTextNode(r.znajdz(text, "\\bFAKTURA VAT NR+.*")));

            //Sprzedawca nip
            System.out.println("Sprzedawca NIP: " + r.znajdz(text, "\\b(Sprzedawca|NIP.*\\d{10})"));
            sprzedawcaNip.appendChild(document.createTextNode(r.znajdz(text, "\\b(Sprzedawca|NIP.*\\d{10})")));

            //Nabywca nip
            System.out.println("Nabywca NIP: " + r.znajdz(text, "\\b(Nabywca|NIP.*\\d*)"));
            nabywcaNip.appendChild(document.createTextNode(r.znajdz(text, "\\b(Nabywca|NIP.*\\d*)")));

            //Termin
            System.out.println("Termin: " + r.znajdz(text, "\\b(Termin.*\\d*)"));
            termin.appendChild(document.createTextNode(r.znajdz(text, "\\b(Termin.*\\d*)")));

            //Data wystawienia
            System.out.println("Data wykonania: " + r.znajdz(text, "\\bData\\s\\bwykonania.*\\b"));

            //Brutto
            System.out.println("Cena brutto: " + r.znajdz(text, "\\d*\\,\\d*"));
            cenaBrutto.appendChild(document.createTextNode(r.znajdz(text, "\\d*\\,\\d*")));

            //tworzenie xml
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

            System.out.println("Done creating XML File");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

}


public class Main {

    public static void main(String[] args) {
        PDFManager pdfManager = new PDFManager();
        pdfManager.setFilePath("003-05-2020_Rida Software.pdf");
        XMLManager xmlManager = new XMLManager();

        try {
            String text = pdfManager.selectTextFromPDF();
            String adjustedText = text.replaceAll("(?m)^[ \t]*\r?\n", "");  //usuwanie pustych linii
            xmlManager.createXMLFile(adjustedText, "file.xml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

